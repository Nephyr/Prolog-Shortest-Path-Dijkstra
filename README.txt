%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           MEMBRI GRUPPO PROGETTO                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%    NOME/COG:  Luca Samuele Vanzo  (735924)                                   %
%    NOME/COG:  Stefano Secci       (756610)                                   %
%    NOME/COG:  Marco Piovani       (735380)                                   %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                      INFORMAZIONI/RICHIESTE PROGETTO                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%    Calcolare il percorso più breve da un punto a un altro di una mappa       %
%    è un problema più che noto. Vi sono diversi algoritmi in grado di         %
%    risolvere questo problema, noto in letteratura come il “Single Source     %
%    Shortest Path Problem” (SSSP Problem, cfr., [CLR+09] capitolo 24).        %
%    Lo scopo di questo progetto è l’implementazione dell’algoritmo di         %
%    Dijkstra (cfr., [CLR+09] 24.3), che risolve il problema SSSP per          %
%    grafi diretti con distanze tra vertici non negative.                      %
%                                                                              %
%    Per procedere all’implementazione di quest’algoritmo è necessaria – e,    %
%    di fatto, è la parte principale del progetto – produrre una               %
%    implementazione di un MINHEAP (o MINPRIORITYQUEUE).                       %
%                                                                              %
%    Inoltre, possiamo pensare di inserire degli altri dati nella base-dati    %
%    Prolog; ad esempio, possiamo inserire informazioni riguardanti associate  %
%    ad ogni vertice mediante dei predicati che rappresentano queste           %
%    associazioni. Ad esempio, potremmo supporre che ad ogni vertice sia       %
%    associata una posizione su una mappa.                                     %
%                                                                              %
%    Una volta scelta una rappresentazione in memoria di un grafo (diretto)    %
%    è semplice manipolare i grafi in Prolog e costruire delle API che ci      %
%    permettono di costruire algoritmi più complessi, quali l’algoritmo        %
%    di Dijkstra per la soluzione del problema SSSP.                           %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%   Il progetto SSSP è stato principalmente organizzato in nove macro parti:   %
%                                                                              %
%   - Verifica esistenza predicati nel database.                               %
%   - Creazione e manipolazione grafi.                                         %
%   - Conversione in liste di vertici, archi e figli (V -> U1, V -> U2, ...)   %
%   - Creazione e manipolazione MinHeap                                        %
%   - Mantenimento delle MinHeap-Property                                      %
%   - Creazione e manipolazione predicati di supporto Dijkstra                 %
%   - Algoritmo per generazione cammini minimi con Dijkstra                    %
%   - Algoritmo per risoluzione cammini minimi a partire da Dijkstra           %
%   - Predicati di listing per grafi, vertici, archi, heap e dijkstra(debug)   %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                       VERIFICA ESISTENZA PREDICATI                           %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Questa macroparte permette la verifica a più step dei predicati presenti o %
%   meno nel database prolog. Ad esempio potrei verificare se nel database     %
%   esiste almeno un dato predicato foo/X.                                     %
%   Non solo, passo successivo, posso verificate se esiste un predicato        %
%   foo( ... ) avente [X, Y, Z, ...] parametri.                                %
%   Inoltre viene definito un valore costante per INFINITE/1.                  %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                   CREAZIONE E MANIPOLAZIONE DEI GRAFI                        %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   La seguente porzione di listato è composta da cinque predicati:            %
%                                                                              %
%       - NEW_GRAPH/1                                                          %
%       - DELETE_GRAPH/1                                                       %
%       - ADD_VERTEX/2                                                         %
%       - ADD_ARC/3                                                            %
%       - ADD_ARC/4                                                            %
%                                                                              %
%   Grazie ad essi è possibile creare, eliminare e manipolare un grafo di N    %
%   vertici aventi M archi orientati. tramite questi predicati, sono stati     %
%   effettuati alcuni accorgimenti: l'inserimento di un grafo (NEW_GRAPH/1),   %
%   impedisce l'assertamento di un nuovo grafo se questo esista già all'interno%
%   del DB. Se non esiste, l'inserimento avrà successo.                        %
%   L'aggiunta di vertici nel DB prolog avviene se esiste il grafo specificato %
%   nel database ed inoltre impedisce l'aggiunta di vertici già esistenti      %
%   appartenenti allo stesso grafo.                                            %
%   La rimozione (DELETE_GRAPH/1) permette di eliminare il grafo specificato   %
%   all'interno del DB,                                                        %
%   ovviamente verificandone sempre la sua esistenza all'interno del DB;       %
%   l'eliminazione del grafo comporterà l'eliminazione dei vertici e degli     %
%   archi che vi appartengono.                                                 %
%   La differenza principale tra ADD_ARC/3 ed ADD_ARC/4 consiste nel porre la  %
%   distanza tra U -> V a 1 con il predicato di arità 3(parametri). Se si      %
%   cerca di inserire archi già esistenti per uno stesso grafo, questi non     %
%   verranno inseriti.                                                         %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%    CONVERSIONE IN LISTE DI VERTICI, ARCHI E FIGLI (V -> U1, V -> U2, ...)    %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%   La seguente porzione di listato permette la conversione in lista di più    %
%   predicati aventi lo stesso pattern di ricerca: foo(X) => [foo(X), ...]     %
%   Inoltre permette di ottenere in forma di lista tutti gli archi,            %
%   dunque arc(G, V, U, Weight), aventi origine nel vertice V.                 %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                  CREAZIONE E MANIPOLAZIONE MINHEAP                           %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   La seguente porzione di listato è composta da otto predicati:              %
%                                                                              %
%       - NEW_HEAP/1                                                           %
%       - HEAP_SIZE/2                                                          %
%       - EMPTY/1                                                              %
%       - NOT_EMPTY/1                                                          %
%       - INSERT/3                                                             %
%       - EXTRACT/3                                                            %
%       - HEAD/3                                                               %
%       - MODIFY_KEY/4                                                         %
%                                                                              %
%   La MinHeap-Property è mantenuta applicando l'algoritmo Heapify su          %
%   predicato EXTRACT/3 dalla root, applicando DecreaseKey su predicato        %
%   INSERT/3 da Size e, infine, applicando DecreaseKey (se nuova chiave è      %
%   minore della precedente) oppure Heapify (se nuova chiave è maggiore        %
%   della precedente) partendo dal nodo di lavoro.                             %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                PREDICATI DI MANTENIMENTO MINHEAP-PROPERTY                    %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%   La seguente porzione di listato permette, mediante algoritmi Heapify e     %
%   DecreaseKey, di mantenere inalterate post extract, insert o modify_key     %
%   le proprietà del MinHeap: P0 -> chiave minima, PSize -> chiave max.        %
%                                                                              %
%                                                                              %
%                                   MINHEAP                                    %
%                                 [ 1, R, 4 ]                                  %
%                                  /       \                                   %
%                                 /         \                                  %
%                        [ 2, F, 6 ]       [ 3, F, 10 ]                        %
%                         /      |           |       \                         %
%                        /       |           |        \                        %
%               [ 4, F, 8 ] [ 5, F, 9 ] [ 6, F, 15 ] [ 7, F, 18 ]              %
%                                                                              %
%                                   VETTORE                                    %
%                           [ 1, 2, 3, 4, 5, 6, 7 ]                            %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%          CREAZIONE E MANIPOLAZIONE PREDICATI DI SUPPORTO DIJKSTRA            %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%   La seguente porzione di listato permette la creazione e manipolazione      %
%   dei predicati generabili da SSSP, ovvero algoritmo di Dijkstra, per        %
%   poi poter calcolare il cammino minimo da Source a un vertice Dest.         %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%          ALGORITMO PER GENERAZIONE CAMMINI MINIMI CON DIJKSTRA               %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%   La seguente porzione di listato permette di calcolare il cammino minimo    %
%   da un vertice Source a un vertice Dest partendo da un grafo G avente       %
%   tutti i vertici con chiave in MinHeap iniziale pari a INFINITE/1.          %
%                                                                              %
%   Inoltre grazie al rilassamento è possibile collassare cammini              %
%   alternativi non minimi rispetto ad altri scoperti durante esecuzione.      %
%                                                                              %
%                                                                              %
%   [ 1, S, 4 ]                                [ 5, R, 17 ] --- [ 6, E, 23 ]   %
%            \                                  /                              %
%           [ 2, R, 6 ] --- [ 3, R, 10 ]       /                               %
%                                     \       /          SSSP - DIJKSTRA       %
%                                    [ 4, R, 11 ]                              %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%      ALGORITMO PER GENERAZIONE CAMMINI MINIMI A PARTIRE DA DIJKSTRA          %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%   La seguente porzione di listato permette di risolvere un cammino minimo    %
%   da un vertice Source a un vertice Dest partendo dai predicati generati     %
%   precedentemente mediante algoritmo di Dijkstra in SSSP/2.                  %
%                                                                              %
%                                                                              %
%   [ 1, S, 4 ]                                [ 5, R, 17 ] --- [ 6, E, 23 ]   %
%            \                                  /                              %
%           [ 2, R, 6 ] --- [ 3, R, 10 ]       /                               %
%                                     \       /          SSSP - DIJKSTRA       %
%                                    [ 4, R, 11 ]                              %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%   PREDICATI DI LISTING PER GRAFI, VERTICI, ARCHI, HEAP E DIJKSTRA (DEBUG)    %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%   La seguente porzione di listato permette di eseguire il LISTING in         %
%   console prolog di vertici, archi, grafi e predicati Dijkstra (debug).      %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
